package jenkinstests.tooploox.com;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ExampleUnitTest {

    @Test
    public void passingTest() throws Exception {
        assertEquals(4, 2 + 2);
    }

    @Test
    public void failingTest() throws Exception {
        assertEquals(5, 2 + 2);
    }
}